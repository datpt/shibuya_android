package com.shibuya.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

    private SharedPreferences mAppSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public SharedPreferencesUtils(Context context){
        mAppSharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        mEditor = mAppSharedPreferences.edit();
    }

    public String getString(String key, String defValue) {
        return mAppSharedPreferences.getString(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return mAppSharedPreferences.getInt(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return mAppSharedPreferences.getFloat(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return mAppSharedPreferences.getLong(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return mAppSharedPreferences.getBoolean(key, defValue);
    }

    public void putString(String key, String value) {
        mEditor.putString(key, value);
    }

    public void putInt(String key, int value) {
        mEditor.putInt(key, value);
    }

    public void putFloat(String key, float value) {
        mEditor.putFloat(key, value);
    }

    public void putLong(String key, long value) {
        mEditor.putLong(key, value);
    }

    public void putBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value);
    }

    public void commit(){
        mEditor.commit();
    }
}
