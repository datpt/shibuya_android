package com.shibuya.config;

public class Define {

    /**
     * Start SharedPreferences Key
     */
    public static final String IS_FIRTS_LAUNCH = "is_first_launch";
    /** End SharedPreferences Key */

    /**
     * Start Request parameters
     */
    public static final String PARAM_CATEGORY = "category";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_PAGE = "page";
    /** End Request parameters */

    /**
     * Start Response keys
     */
    public static final String KEY_CONTENT_INFO_LIST = "content_info_list";
    /** End Response keys */

    /**
     * Start Constant Category
     */
    public static final int CATEGORY_RECOMMENDED = 1;
    public static final int CATEGORY_EVENT = 2;
    public static final int CATEGORY_SHIBUYA = 3;
    /** End Constant Category */

}
