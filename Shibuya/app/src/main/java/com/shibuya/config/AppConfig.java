package com.shibuya.config;

import com.shibuya.network.API;

public class AppConfig {

    public static final boolean DEBUG = true;

    public static String getBaseUrl() {
        if (DEBUG) {
            return API.DEV_BASE_URL;
        } else {
            return API.BASE_URL;
        }
    }


}
