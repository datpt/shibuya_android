package com.shibuya.model;

import java.util.List;

/**
 * Created by tungtt on 3/16/2016.
 */
public class EventItem {

    private String content_id;
    private String content_name;
    private String content_type;
    private String explain_detail;
    private String latitude;
    private String longitude;
    private String settlement_flg;
    private String price;
    private String price_currency;
    private String price_per_count;
    private String start_season;
    private String end_season;
    private String rating;
    private String pick_up_flag;
    private String favorite_num;
    private String save_num;
    private String watch_num;
    private List<Image> image_list;

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getContent_name() {
        return content_name;
    }

    public void setContent_name(String content_name) {
        this.content_name = content_name;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getExplain_detail() {
        return explain_detail;
    }

    public void setExplain_detail(String explain_detail) {
        this.explain_detail = explain_detail;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSettlement_flg() {
        return settlement_flg;
    }

    public void setSettlement_flg(String settlement_flg) {
        this.settlement_flg = settlement_flg;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_currency() {
        return price_currency;
    }

    public void setPrice_currency(String price_currency) {
        this.price_currency = price_currency;
    }

    public String getPrice_per_count() {
        return price_per_count;
    }

    public void setPrice_per_count(String price_per_count) {
        this.price_per_count = price_per_count;
    }

    public String getStart_season() {
        return start_season;
    }

    public void setStart_season(String start_season) {
        this.start_season = start_season;
    }

    public String getEnd_season() {
        return end_season;
    }

    public void setEnd_season(String end_season) {
        this.end_season = end_season;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPick_up_flag() {
        return pick_up_flag;
    }

    public void setPick_up_flag(String pick_up_flag) {
        this.pick_up_flag = pick_up_flag;
    }

    public String getFavorite_num() {
        return favorite_num;
    }

    public void setFavorite_num(String favorite_num) {
        this.favorite_num = favorite_num;
    }

    public String getSave_num() {
        return save_num;
    }

    public void setSave_num(String save_num) {
        this.save_num = save_num;
    }

    public String getWatch_num() {
        return watch_num;
    }

    public void setWatch_num(String watch_num) {
        this.watch_num = watch_num;
    }

    public List<Image> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<Image> image_list) {
        this.image_list = image_list;
    }
}
