package com.shibuya.network;

import com.shibuya.config.AppConfig;
import com.shibuya.config.Define;

import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class RequestUtils {

    public void getEvents(Callback callback, int category, int limit, int page) {

        String url = AppConfig.getBaseUrl() + API.GET_CONTENT_LIST_API;

        RequestBody requestBody = new FormBody.Builder().add(
                Define.PARAM_CATEGORY,
                String.valueOf(category)
        ).add(
                Define.PARAM_LIMIT,
                String.valueOf(limit)
        ).add(
                Define.PARAM_PAGE,
                String.valueOf(page)
        ).build();

        final Request request = new Request.Builder().url(url).post(requestBody).build();

        OkHttpClient client = new OkHttpClient();

        client.newCall(request).enqueue(callback);
    }
}
