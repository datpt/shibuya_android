package com.shibuya.network;

public class API {

    public static final String BASE_URL = "";

    public static final String DEV_BASE_URL = "http://prefapp-672114432.ap-northeast-1.elb.amazonaws.com";

    public static final String GET_CONTENT_LIST_API = "/api/getContentList";

    public static final String GET_CONTENTS_DETAIL_API = "/api/getContentsDetail";
}
