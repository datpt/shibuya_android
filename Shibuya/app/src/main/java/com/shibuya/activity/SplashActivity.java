package com.shibuya.activity;

import android.animation.Animator;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.shibuya.R;
import com.shibuya.ShibuyaApplication;
import com.shibuya.activity.base.BaseActivity;
import com.shibuya.config.Define;
import com.shibuya.custom.animation.BlurAnimation;
import com.shibuya.util.SharedPreferencesUtils;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

public class SplashActivity extends BaseActivity implements View.OnClickListener {

    public static final int REQUEST_ENABLE_BT = 100;

    private ImageView ivEvent, ivLogo;
    private LinearLayout llLanguageSetting, llFacebookLogin;
    private RelativeLayout rlController;
    private Button btnLanguageJP, btnLanguageEN, btnLanguageCN, btnLanguageCNSimple, btnLanguageKR, btnSkip;
    private LoginButton loginButton;

    private Animator.AnimatorListener fadeInListener, translationListener;
    private Animation.AnimationListener fadeOutListener;

    private CallbackManager callbackManager;
    private FacebookCallback<LoginResult> facebookCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_splash);
        initView();
        initAnimation();
        checkBlueTooth();
    }

    // initialize all view
    private void initView() {
        ivEvent = (ImageView) findViewById(R.id.iv_event_image);
        ivLogo = (ImageView) findViewById(R.id.iv_logo);
        llLanguageSetting = (LinearLayout) findViewById(R.id.ll_language_setting);
        llFacebookLogin = (LinearLayout) findViewById(R.id.ll_facebook_login);
        rlController = (RelativeLayout) findViewById(R.id.rl_controller);
        btnLanguageJP = (Button) findViewById(R.id.btn_language_jp);
        btnLanguageEN = (Button) findViewById(R.id.btn_language_en);
        btnLanguageCN = (Button) findViewById(R.id.btn_language_cn);
        btnLanguageCNSimple = (Button) findViewById(R.id.btn_language_cn_simple);
        btnLanguageKR = (Button) findViewById(R.id.btn_language_kr);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnLanguageJP.setOnClickListener(this);
        btnLanguageEN.setOnClickListener(this);
        btnLanguageCN.setOnClickListener(this);
        btnLanguageCNSimple.setOnClickListener(this);
        btnLanguageKR.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        initFacebookLogin();
    }

    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.btn_facebook_login);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_location", "user_birthday"));
        facebookCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("FacebookLogin", "onSuccess()");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.e("FacebookLogin", "User Profile: " + object.toString());
                                // Process login facebook through API
                                finish();
                                Intent i = new Intent(SplashActivity.this, TermActivity.class);
                                startActivity(i);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,birthday,email,location,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("FacebookLogin", "onCancel()");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e("FacebookLogin", "onError()");
            }
        };
        loginButton.registerCallback(callbackManager, facebookCallback);
    }

    private void initAnimation() {
//        fadeOutListener = new Animator.AnimatorListener() {
//
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                rlController.setVisibility(View.VISIBLE);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (ivLogo != null) {
//                            float y = llLanguageSetting.getY();
//                            float marginBot = getResources().getDimension(R.dimen.logo_magin_bottom);
//                            ivLogo.animate().translationY(y - (marginBot + ivLogo.getHeight())).setDuration(1000).setListener(translationListener);
//                        }
//                    }
//                }, 500);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        };

        fadeOutListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rlController.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (ivLogo != null) {
                            float y = llLanguageSetting.getY();
                            float marginBot = getResources().getDimension(R.dimen.logo_magin_bottom);
                            ivLogo.animate().translationY(y - (marginBot + ivLogo.getHeight())).setDuration(1000).setListener(translationListener);
                        }
                    }
                }, 500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        translationListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (llLanguageSetting != null) {
                    llLanguageSetting.animate().alpha(1.0f).setDuration(500).setListener(fadeInListener);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        fadeInListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                btnLanguageJP.setEnabled(true);
                btnLanguageEN.setEnabled(true);
                btnLanguageCN.setEnabled(true);
                btnLanguageCNSimple.setEnabled(true);
                btnLanguageKR.setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
    }

    private void checkBlueTooth(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            perform();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                showBluetoothDialog();
            } else {
                perform();
            }
        }
    }

    private void perform() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferencesUtils mSharedPreferencesUtils = new SharedPreferencesUtils(SplashActivity.this);
                if (mSharedPreferencesUtils.getBoolean(Define.IS_FIRTS_LAUNCH, true)) {
                    mSharedPreferencesUtils.putBoolean(Define.IS_FIRTS_LAUNCH, false);
                    mSharedPreferencesUtils.commit();
                    if (ivEvent != null) {
                        BlurAnimation blurAnimation = new BlurAnimation(ivEvent, 3.75f);
                        blurAnimation.setDuration(1000);
                        blurAnimation.setAnimationListener(fadeOutListener);
                        ivEvent.startAnimation(blurAnimation);
//                    ivEvent.animate().alpha(0.55f).setDuration(1000).setListener(fadeOutListener);
                    }
                } else {
                    finish();
                    Intent i = new Intent(SplashActivity.this, TopActivity.class);
                    startActivity(i);
                }
            }
        }, 1000);
    }

    private void showBluetoothDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setMessage(R.string.dialog_bluetooth_message)
                .setTitle(R.string.dialog_bluetooth_title)
                .setNegativeButton(R.string.dialog_bluetooth_btn_later, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        perform();
                    }
                }).setPositiveButton(R.string.dialog_bluetooth_btn_setting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intentBtEnabled = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(intentBtEnabled, REQUEST_ENABLE_BT);
                    }
                });
        builder.create().show();
    }

    private void disableAllButtonLanguage() {
        btnLanguageJP.setEnabled(false);
        btnLanguageEN.setEnabled(false);
        btnLanguageCN.setEnabled(false);
        btnLanguageCNSimple.setEnabled(false);
        btnLanguageKR.setEnabled(false);
    }

    private void showFacebookLogin() {
        llLanguageSetting.setVisibility(View.INVISIBLE);
        llFacebookLogin.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            perform();
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_language_jp:
                disableAllButtonLanguage();
                ShibuyaApplication.setAppLocale(Locale.JAPAN);
                showFacebookLogin();
                break;
            case R.id.btn_language_en:
                disableAllButtonLanguage();
                ShibuyaApplication.setAppLocale(Locale.ENGLISH);
                showFacebookLogin();
                break;
            case R.id.btn_language_cn:
                disableAllButtonLanguage();
                ShibuyaApplication.setAppLocale(Locale.TRADITIONAL_CHINESE);
                showFacebookLogin();
                break;
            case R.id.btn_language_cn_simple:
                disableAllButtonLanguage();
                ShibuyaApplication.setAppLocale(Locale.SIMPLIFIED_CHINESE);
                showFacebookLogin();
                break;
            case R.id.btn_language_kr:
                disableAllButtonLanguage();
                ShibuyaApplication.setAppLocale(Locale.KOREA);
                showFacebookLogin();
                break;
            case R.id.btn_skip:
                Intent i = new Intent(SplashActivity.this, TermActivity.class);
                startActivity(i);
                finish();
                break;
        }
    }
}
