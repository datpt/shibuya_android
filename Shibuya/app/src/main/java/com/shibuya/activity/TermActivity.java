package com.shibuya.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.shibuya.R;
import com.shibuya.activity.base.BaseActivity;

public class TermActivity extends BaseActivity implements View.OnClickListener {

    private Button btnAgreeTerm;
    private WebView wvTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        initView();
    }

    private void initView() {
        btnAgreeTerm = (Button) findViewById(R.id.btn_agree_term);
        wvTerm = (WebView) findViewById(R.id.wv_term);
        wvTerm.getSettings().setLoadWithOverviewMode(true);
        wvTerm.getSettings().setUseWideViewPort(true);
        wvTerm.setWebViewClient(new WebViewClient());
        btnAgreeTerm.setOnClickListener(this);
        wvTerm.loadUrl("http://google.com.vn");
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, TopActivity.class);
        startActivity(i);
        finish();
    }
}
