package com.shibuya.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.shibuya.R;
import com.shibuya.activity.base.BaseAppCompatActivity;
import com.shibuya.activity.fragment.CouponFragment;
import com.shibuya.activity.fragment.ListEventFragment;
import com.shibuya.config.Define;
import com.shibuya.custom.adapter.TopScreenViewPagerAdapter;

public class TopActivity extends BaseAppCompatActivity {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private FloatingActionButton mFabSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);

        setupToolBar();

    }

    private void setupToolBar() {

        mToolbar = (Toolbar) findViewById(R.id.tb_top_screen);

        mToolbar.setBackgroundColor(getResources().getColor(R.color.tool_bar));

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        setupTabLayout();
    }

    private void setupViewPager() {

        mViewPager = (ViewPager) findViewById(R.id.vp_top_screen);

        TopScreenViewPagerAdapter adapter
                = new TopScreenViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(ListEventFragment.newInstance(Define.CATEGORY_RECOMMENDED),
                getResources().getString(R.string.recommended_tab));
        adapter.addFragment(ListEventFragment.newInstance(Define.CATEGORY_EVENT),
                getResources().getString(R.string.event_tab));
        adapter.addFragment(ListEventFragment.newInstance(Define.CATEGORY_SHIBUYA),
                getResources().getString(R.string.shibuya_tab));
        adapter.addFragment(new CouponFragment(),
                getResources().getString(R.string.coupon_tab));

        mViewPager.setAdapter(adapter);
    }

    private void setupTabLayout() {

        mTabLayout = (TabLayout) findViewById(R.id.tl_top_screen);

        setupViewPager();

        mTabLayout.setupWithViewPager(mViewPager);
    }

    public void onFabSettingClick(View view) {
        Intent intent = new Intent(TopActivity.this, SettingActivity.class);
        startActivity(intent);
    }
}
