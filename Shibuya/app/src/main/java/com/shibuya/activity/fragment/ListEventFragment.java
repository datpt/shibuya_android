package com.shibuya.activity.fragment;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.shibuya.R;
import com.shibuya.config.Define;
import com.shibuya.custom.adapter.EventListAdapter;
import com.shibuya.model.EventItem;
import com.shibuya.network.RequestUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by tungtt on 3/15/2016.
 */
public class ListEventFragment extends Fragment implements Callback {

    private int mPage;

    private int mCategory;

    private Activity mActivity;

    private ImageView mListLoadingIcon;

    private RecyclerView mRecommendedList;

    private EventListAdapter mAdapter;

    private List<EventItem> mEventItems = new ArrayList<>();

    public static ListEventFragment newInstance(int category) {
        ListEventFragment fragment = new ListEventFragment();
        Bundle args = new Bundle();
        args.putInt(Define.PARAM_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mPage = 1;
        if (getArguments() != null) {
            mCategory = getArguments().getInt(Define.PARAM_CATEGORY, Define.CATEGORY_RECOMMENDED);
        } else {
            mCategory = Define.CATEGORY_RECOMMENDED;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        mListLoadingIcon = (ImageView) view.findViewById(R.id.iv_event_list_loading_icon);

        mRecommendedList = (RecyclerView) view.findViewById(R.id.rv_event_list);

        mAdapter = new EventListAdapter(
                mEventItems,
                getActivity().getApplicationContext()
        );

        RecyclerView.LayoutManager layoutManager
                = new LinearLayoutManager(getActivity().getApplicationContext());

        mRecommendedList.setLayoutManager(layoutManager);

        mRecommendedList.setItemAnimator(new DefaultItemAnimator());

        mRecommendedList.setAdapter(mAdapter);

        loadData();

        return view;
    }

    private void loadData() {
        // Show progress dialog
        showListLoadingIcon();
        RequestUtils requestUtils = new RequestUtils();
        requestUtils.getEvents(this, mCategory, 5, mPage);
    }

    private void showListLoadingIcon() {
        if (mAdapter.getItemCount() == 0) {
            mListLoadingIcon.setVisibility(View.VISIBLE);
            mListLoadingIcon.setBackgroundResource(R.drawable.loading_icon_anim);
            AnimationDrawable animation = (AnimationDrawable) mListLoadingIcon.getBackground();
            animation.start();
        }
    }

    private void hideListLoadingIcon() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mListLoadingIcon.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onFailure(Call call, IOException e) {
        hideListLoadingIcon();
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {

        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }

        String resultData = response.body().string();

        List<EventItem> listEvents = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject(resultData);

            listEvents = Arrays.asList(
                    new Gson().fromJson(jsonObject.getString(Define.KEY_CONTENT_INFO_LIST),
                            EventItem[].class)
            );

            if (mEventItems != null) {
                mEventItems.clear();
            } else {
                mEventItems = new ArrayList<>();
            }

            mEventItems.addAll(listEvents);

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                }
            });

            hideListLoadingIcon();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
