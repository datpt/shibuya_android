package com.shibuya.custom.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.shibuya.R;
import com.shibuya.model.EventItem;

import java.util.List;

/**
 * Created by tungtt on 3/16/2016.
 */
public class EventListAdapter extends
        RecyclerView.Adapter<EventListAdapter.ViewHolder> {

    private Context mContext;

    private List<EventItem> mEventItems;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mIvPhoto, mIvLoadingIcon;
        public TextView mTvTitle, mTvLeadSentence;

        public ViewHolder(View view) {
            super(view);

            mIvPhoto = (ImageView) view.findViewById(R.id.iv_event_item_photo);

            mIvLoadingIcon = (ImageView) view.findViewById(R.id.iv_event_item_loading_icon);

            mTvTitle = (TextView) view.findViewById(R.id.tv_event_item_title);

            mTvLeadSentence = (TextView) view.findViewById(R.id.tv_event_item_lead_sentence);
        }
    }

    public EventListAdapter(List<EventItem> eventItems, Context context) {
        this.mEventItems = eventItems;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.item_event_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        EventItem item = mEventItems.get(position);

        ImageLoader imageLoader = ImageLoader.getInstance();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.color.event_item_background_photo)
                .showImageForEmptyUri(R.color.event_item_background_photo)
                .showImageOnFail(R.color.event_item_background_photo)
                .delayBeforeLoading(1000)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        imageLoader
                .displayImage(item.getImage_list().get(0).getImage_url(), holder.mIvPhoto,
                        options,
                        new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                                holder.mIvLoadingIcon.setVisibility(View.VISIBLE);

                                holder.mIvLoadingIcon
                                        .setBackgroundResource(R.drawable.loading_icon_anim);

                                AnimationDrawable animation
                                        = (AnimationDrawable) holder.mIvLoadingIcon.getBackground();

                                animation.start();
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                holder.mIvLoadingIcon.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                holder.mIvLoadingIcon.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                holder.mIvLoadingIcon.setVisibility(View.GONE);
                            }
                        });

        holder.mTvTitle.setText(item.getContent_name());

        holder.mTvLeadSentence.setText(item.getExplain_detail());
    }

    @Override
    public int getItemCount() {
        return mEventItems.size();
    }
}
